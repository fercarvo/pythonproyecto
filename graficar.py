import os
import nltk
import re
from nltk.corpus import stopwords
from bs4 import BeautifulSoup  
from nltk.stem import *
import unicodedata
from collections import Counter
from collections import OrderedDict
import math
import matplotlib.pyplot as plt
import numpy as np
import shutil


#EL counter es una estructura
#Cuando a un cunter le aplico most_commomn() lo trasformo en una lista de tuplas de 2 valores cada tupla
#

def textoGrafico2():
    plt.ylabel('Documentos')
    plt.xlabel('Numero de terminos')
    plt.title('Grafico del numero de terminos por documento')
    plt.grid(True)
    plt.savefig("terminosxdoc.png") #Guarda grafico de barras
    plt.show()

def graficar(diccionario):
    labels = []
    valores = []
    for label, valor in diccionario:
        labels.append(str(label))
        valores.append(valor)
    indexes = np.arange(len(labels))
    plt.barh(indexes,valores,1)
    plt.yticks(indexes + 1 * 0.5, labels)
    textoGrafico2()


def tF(palabra, listaPalabras):
    for word, numero in Counter(listaPalabras).iteritems():
        if palabra==word:
            return numero
    return 0

def eliminarPalabras(palabrasAeliminar, listaPalabras):
    for palabra in palabrasAeliminar:
        while palabra in listaPalabras:
            listaPalabras.remove(palabra)  



def comunes(listaPalabras, numero):    
    mascomunes = Counter(listaPalabras).most_common(numero)
    return mascomunes
    #d_sorted_by_value = OrderedDict(sorted(dict(mascomunes).items(), key=lambda x: x[1]))
    #return d_sorted_by_value          


List = [1,2,3,4,5,6,4,3,4,5,7,5,4,3,3,44,4,3,3]
palabras = ['hola', 'chao','chao','hola','hola','hola','hola','hola','a','b','c']
eliminar = ['hola', 'fer']

diccionario = {}

diccionario['uno']=45
diccionario['casa']=100000

print palabras
eliminarPalabras(eliminar, palabras)
#primeros = palabras[0:3]
counter = Counter(palabras)
#print comunes(palabras, 2)
print palabras
print counter
print diccionario
print diccionario.items()
f=0.99
d=9.544
print ("Texto con valores %f y tambien un " %f + "%f" %f)
#graficar(counter)
import os
import nltk
import re
from nltk.corpus import stopwords
from bs4 import BeautifulSoup  
from nltk.stem import *
import unicodedata
from collections import Counter
from collections import OrderedDict
import math
import matplotlib.pyplot as plt
import numpy as np
import shutil
 
#Recibe un str de la ruta de origen, el destino se ubica donde este la ruta del proyecto           
def copiarOrigenToDestino(origen):
    lista_archivos = []
    #origen = 'C:\Users\Anny\Desktop\origen'
    destino = '/home/fernando/Escritorio/PythonProyecto/'
    
    if not os.path.isdir(destino): #asegurar exitencia de la carpeta
        os.mkdir(destino)
    extensiones = ['.txt','.csv']
    
    lista_archivos =  [ [os.path.join(root,file),root.split(origen)[1]] 
                    for root,dirs,files in os.walk(origen) 
                    for file in files if os.path.splitext(file)[-1] in extensiones ]
    
    carpeta_destino = os.path.join(destino,'copias')
    os.mkdir(carpeta_destino)

    for archivo in lista_archivos:
        carpeta_fichero = carpeta_destino + archivo[1]
        if not os.path.isdir(carpeta_fichero): #si no se a existe se crea
            os.mkdir(carpeta_fichero)
        shutil.copy(archivo[0], carpeta_fichero)


def nombreArchivos():
    carpeta = '/home/fernando/Escritorio/PythonProyecto/copias/'
    #carpeta = 'C:\Users\Anny\Desktop\PythonProyecto\pythonproyecto\copias'
    for root,dirs,files in os.walk(carpeta):  
        files = files
    return files


''' ***********************************************************************************
    ESTA FUNCION ES LA QUE HACE TODO EL PROCESAMIENTO
    Devuelve leerArchivo[0] (lista), lista de palabras corpus
    Devuelve leerArchivo[1] (diccionario), diccionario de documentos
    Devuelve leerArchivo[2] (lista), lista de steamming corpus
    Devuelve leerArchivo[3] (diccionario), diccionario de steamming 
'''  
def leerArchivo():
    palabrasCorpus = [] #lista de palabras del corpus
    dictionaryDocs = {} #key=nombre del doc, contenido=lista de sus palabras
    stemmingCorpus = [] #lista de stemming del corpus
    dictionaryStemmingDocs = {}  #key=nombre del doc, contenido=lista de sus stemming
    ruta = '/home/fernando/Escritorio/PythonProyecto/copias/'
    #ruta = "C:\Users\Anny\Desktop\PythonProyecto\pythonproyecto\copias\\"
    nameFile = nombreArchivos()
    contador = Counter()
    for documento in nameFile:
        palabrasDocumento = [] #aqui se guardan las palabras de documento
        stemmingDocumento = [] #aqui se guardan las palabras stemming de documento
        try:
            archivo = open(ruta+documento,'r')
        except FileNotFoundError:
            archivo.close()
            print ("Archivo no existe")
        registros = archivo.read()
        review = revisarPalabras(registros)
        
        for registro in review.split():
            registroStemming = SnowballStemmer("spanish").stem(registro)

            palabrasCorpus.append(str(registro))  #Lista de palabras del corpus
            palabrasDocumento.append(str(registro)) #Lista de palabras por documento
            stemmingCorpus.append(str(registroStemming))  #lista de steamming del corpus
            stemmingDocumento.append(str(registroStemming)) #Lista de stemming por documento

        dictionaryDocs[documento] = palabrasDocumento
        dictionaryStemmingDocs[documento] = stemmingDocumento
        archivo.close()
        
    return (palabrasCorpus, dictionaryDocs, stemmingCorpus, dictionaryStemmingDocs) 

''' ***********************************************************************************
    Recive= registros
    Devuelve= texto limpio en formato unicode
'''      
def revisarPalabras( registros ):
    revisionTexto = BeautifulSoup(registros,"html.parser").get_text() 
    convTilde = ''.join((c for c in unicodedata.normalize('NFD', revisionTexto) if unicodedata.category(c)!= 'Mn'))
    soloLetras = re.sub("[^a-zA-Z]", " ", convTilde) 
    words = soloLetras.lower().split()                             
    stops = set(stopwords.words("spanish"))                  
    unicodePalabras = [w for w in words if not w in stops]   
    return( " ".join( unicodePalabras ))  


''' ***********************************************************************************
    Recive= listaPalabras, (lista) lista de palabras
    Recive= numero, (int) numero de datos que quiero mostrar 
    Devuelve= (Lista de tuplas) (palabra, numero_veces)
'''  
def comunes(listaPalabras, numero):    
    return Counter(listaPalabras).most_common(numero)

''' Recibe  Corpus, documentos 
            numero, el numero de datos a mostrar
    Devuelve    tupla de las palabras con iDF mas alto, (palabra, iDF)
'''
def iDFmasAlto(corpus, numero):
    list1= []
    for palabra in palabrasDelCorpus(corpus):
        list1.append((palabra, iDF(palabra, corpus[1])))
    list1.sort(key=lambda tup: tup[1], reverse=True)
    return list1[0:numero]

''' Recibe  Corpus, documentos 
            numero, el numero de datos a mostrar
    Devuelve    tupla de las palabras con iDF mas alto con stemming, (palabra, iDF)
'''
def iDFmasAltoStemming(corpus, numero):
    list1= []
    for palabra in palabrasDelCorpus(corpus):
        list1.append((palabra, iDF(palabra, corpus[1])))
    list1.sort(key=lambda tup: tup[1], reverse=True)
    return list1[0:numero]

def stemmingDelCorus(corpus):
    lista= []
    for palabra in corpus[2]:
        if palabra not in lista:
            lista.append(palabra)
    return lista

def palabrasDelCorpus(corpus):
    lista= []
    for palabra in corpus[0]:
        if palabra not in lista:
            lista.append(palabra)
    return lista

#Recibe una lista de palabras y retorna un conter de las palabras con su numero de veces
def contadorPalabras(listaPalabras):
    palabrasContadas = Counter(listaPalabras).most_common()
    return palabrasContadas

''' ***********************************************************************************
    Recive= documentos, (Diccionario) son los documentos del corpus
    Devuelve= (lista de tuplas) (nombre_doc, numero_terminos)
'''
def numeroTerminosXdoc(documentos):
    doc = []
    for nombre, listaPalabras in documentos.iteritems():
        doc.append((nombre, len(contadorPalabras(listaPalabras))))
    return doc

''' ***********************************************************************************
    Recive= documentos, (Diccionario) son los documentos del corpus
    Devuelve= (int) numero de documentos que posee el corpus
'''
def numeroDocumentos(documentos):
    return len(documentos)    

''' ***********************************************************************************
    Recibe= palabra,(string) que hace IDF del corpus
    Recive= documentos, (Diccionario) son los documentos del corpus
    Devuelve= (float) del IDF.
'''
def iDF (palabra, documentos):
    numeroDocs= len(documentos) #numero de documentos
    contadorExistencial = 0     #numero de documentos en que aparece la palabra
    for nombreDoc, palabras in documentos.iteritems():
        if palabra in palabras:
            contadorExistencial= contadorExistencial+1
    if (numeroDocs < contadorExistencial):
        return 0
    else:
        return math.log10(numeroDocs/contadorExistencial)

''' ***********************************************************************************
    Recibe= palabra,(string) palabras a hacer tF_IDF
    Recive= documentos, (Diccionario) son los documentos del corpus con sus palabras
    Devuelve= (Diccionario) del tF_IDF de esa palabra.
'''
def tF_IDF(palabra, documentos):
    dictionartTFIDF={}
    for nombreDoc, listaPalabras in documentos.iteritems():
        dictionartTFIDF[nombreDoc]= tF(palabra, listaPalabras) * iDF(palabra, documentos)
    return dictionartTFIDF

def tF_IDFListaPalabras(listaPalabras, documentos):
    for palabra in listaPalabras:
        print "\nel TF-IDF de %s" %palabra + " es: "
        print tF_IDF(palabra, documentos)


''' ***********************************************************************************
    FUNCION QUE CUENTA EL NUMERO DE VECES QUE UNA PALABRA SE REPITE EN UNA LISTA
    Recibe= palabra, (string) palabra a contar
    Recive= listaPalabras, (Lista) una lista de palabras
    Devuelve= (int) numero de veces que aparece palabra en listaPalabras
'''
'''optimizada'''
def tF(palabra, listaPalabras):
    for word, numero in Counter(listaPalabras).iteritems():
        if palabra==word:
            return numero
    return 0

''' Recibe= diccionario de documentos
    IMPRIME= Por documento, las 10 palabras con TF mas alto
'''
def mostrarTFxDoc(diccionario):
    for nombreDoc, listaPalabras in diccionario.iteritems():
        print "\n********EL DOCUMENTO %s ***********" % nombreDoc
        print comunes(listaPalabras, 10)            
    
''' ***********************************************************************************
    Recibe= diccionario, (Diccionario)
    Devuelve= imprime el diccionario X sobre Y
'''
def graficar(diccionario, texto):
    labels = []
    valores = []
    for label, valor in diccionario:
        labels.append(str(label))
        valores.append(valor)
    indexes = np.arange(len(labels))
    plt.barh(indexes,valores,1)
    plt.yticks(indexes + 1 * 0.5, labels)
    plt.ylabel(texto[0])
    plt.xlabel(texto[1])
    plt.title(texto[2])
    plt.grid(True)
    plt.savefig(texto[2] + ".png") #Guarda grafico de barras
    plt.show()

'''Se le aplica stemming a toda la lista'''
def stemmingLista(palabrasAeliminar):
    lista= []
    for palabra in palabrasAeliminar:
        lista.append(SnowballStemmer("spanish").stem(palabra))
    return lista

''' ***********************************************************************************
    Recibe= corpus
    Recibe= palabrasAeliminar, (lista)
'''
'''OPTIMIZADAAAA'''
def eliminarPalabrasDeArchivos(palabrasAeliminar, corpus):
    eliminarPalabras(palabrasAeliminar, corpus[0]) #de la lista de palabras corpus
    eliminarPalabras(stemmingLista(palabrasAeliminar), corpus[2]) #de la lista de palabras stemming
    for nombreDoc, listaPalabras in corpus[1].iteritems():#dlista de documentos 
        eliminarPalabras(palabrasAeliminar, listaPalabras)
    for nombreDoc, listaPalabras in corpus[3].iteritems():#dista de documentos stemming
        eliminarPalabras(stemmingLista(palabrasAeliminar), listaPalabras)   

''' ***********************************************************************************
    Recibe= palabrasAeliminar(lista), lista de palabras que se quiere eliminar
    Recibe= listaPalabras(lista), lista de palabras donde se encuentran las palabras a eliminar
'''
def eliminarPalabras(palabrasAeliminar, listaPalabras):
    for palabra in palabrasAeliminar:
        while palabra in listaPalabras:
            listaPalabras.remove(palabra)



'''El sub menu de las estadisticas'''    
def sub_estadisticas(corpus, conStemming):
    while True:
        print "\n\n             ESTADISTICAS\n"
        print "     1. Numero de terminos por documentos" 
        print "     2. Numero de documentos en el Corpus"
        print "     3. 50 terminos mas frecuentes"
        print "     4. Salir"
        op = raw_input("Elija una opcion: ")
        if (op == '1'):
            if conStemming:
                graficar(numeroTerminosXdoc(corpus[3]), ("Documentos", "terminos", "Terminos por documento con stemming"))
            else:
                graficar(numeroTerminosXdoc(corpus[1]), ("Documentos", "terminos", "Terminos por documento"))
        if (op == '2'):
            print "\nel numero de documentos es: %d" % numeroDocumentos(corpus[1])
        if (op == '3'):
            if conStemming:
                graficar(comunes(corpus[2], 50), ("Terminos", "repeticiones", "Repeticiones por termino con stemming"))
            else:    
                graficar(comunes(corpus[0], 50), ("Terminos", "repeticiones", "Repeticiones por termino"))
        if (op == '4'):
            break
'''El sub menu de las evaluaciones'''            
def sub_evalucacion(corpus, conStemming, listaDeBuscar):
    while True:
        print "\n\n             EVALUACION DE CONTENIDO\n"
        print "     1. 10 palabras con mayor TF POR documento"
        print "     2. 10 palabras con mayor IDF del corpus"
        print "     3. Tf-idf de las palabras buscadas"
        print "     4. Salir"
        op = raw_input("\nElija una opcion: ")
        if (op == '1'):
            if conStemming:
                mostrarTFxDoc(corpus[3])
            else:
                mostrarTFxDoc(corpus[1])
        if (op == '2'):
            print "PROCESANDO..."
            if conStemming:
                graficar(iDFmasAltoStemming(corpus, 10), ("Terminos", "IDF", "10 palabras con mayor IDF"))
            else:
                graficar(iDFmasAlto(corpus, 10), ("Terminos", "IDF", "10 palabras con mayor IDF"))
        if (op == '3'):
            if len(listaDeBuscar)==0:
                print "Usted no ha ingresado palabras a buscar"
            else:    
                if conStemming:
                    tF_IDFListaPalabras(listaDeBuscar, corpus[3])    
                else:
                    tF_IDFListaPalabras(listaDeBuscar, corpus[1])
        if (op == '4'):
            break
            
def menuprincipal():
    ruta=raw_input("Ingrese ruta: ")
    a=os.path.exists(ruta)
    while(not a):
        ruta=raw_input("Ingrese ruta: ")
        a=os.path.exists(ruta)
    copiarOrigenToDestino(ruta)

    print "\nPROCESANDO...."

    archivo= leerArchivo()

    while True:
        preg="k"
        preg2="k"
        listaBuscar=[]
        listaEliminar=[]
        st= raw_input("\nDesea realizar estadisticas con steamming? : ")
        print st

        if(st == "si"):
            bandera= True
            print             
        else:
            bandera= False

        while(preg != "si" and preg != "no"):
            preg=raw_input("\nDesea eliminar una palabra? : ")
            while(preg == "si"):
                preg="k"
                pEliminar=raw_input("ingrese palabra a eliminar: ")
                listaEliminar.append(pEliminar)
                while(preg != "si" and preg != "no"):
                    preg=raw_input("\nDesea eliminar otra palabra? : ")
            print "PROCESANDO..."
            eliminarPalabrasDeArchivos(listaEliminar, archivo)

            while(preg2 != "si" and preg2 != "no"):
                preg2=raw_input("\nQuiere buscar una palabra? : ")
            while(preg2 == "si"):
                preg2="k"
                pBuscar=raw_input("ingrese palabra a buscar: ")
                if pBuscar in archivo[0]:
                    listaBuscar.append(pBuscar)
                else:
                    print "%s no existe en el corpus"

                while(preg2 != "si" and preg2 != "no"):
                    preg2=raw_input("\nQuiere buscar otra palabra? : ")

        sub_estadisticas(archivo, bandera)
        sub_evalucacion(archivo, bandera, listaBuscar)

menuprincipal()